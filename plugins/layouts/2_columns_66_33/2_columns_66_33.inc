<?php

// Plugin definition
$plugin = array(
  'title' => t('2 Columns: 66 - 33'),
  'category' => t('2 Columns'),
  'icon' => '2_columns_66_33.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_2_columns_66_33($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-xl-size2of3')),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-xl-size1of3')),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
  );
}
