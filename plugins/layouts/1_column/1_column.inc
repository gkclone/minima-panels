<?php

// Plugin definition
$plugin = array(
  'title' => t('1 Column'),
  'category' => t('1 Column'),
  'icon' => '1_column.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_1_column($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['primary'],
      ),
    ),
  );
}
