<?php

/**
 * Renders a minima page layout.
 */
function theme_minima_page_layout($variables) {
  // Remove #theme_wrappers from the layout when rendering in the admin theme.
  if ($variables['renderer']->admin) {
    minima_panels_remove_theme_wrappers_recursive($variables['minima_layout']);
  }

  return drupal_render($variables['minima_layout']);
}

/**
 * Renders a minima site layout.
 */
function theme_minima_site_layout($variables) {
  // Remove #theme_wrappers from the layout when rendering in the admin theme.
  if ($variables['renderer']->admin) {
    minima_panels_remove_theme_wrappers_recursive($variables['minima_layout']);
  }

  return drupal_render($variables['minima_layout']);
}

/**
 * Preprocess variables for theme minima_page_layout.
 */
function template_preprocess_minima_page_layout(&$variables) {
  $variables['minima_layout'] = minima_panels_get_minima_layout($variables);
}

/**
 * Preprocess variables for theme minima_site_layout.
 */
function template_preprocess_minima_site_layout(&$variables) {
  $variables['minima_layout'] = minima_panels_get_minima_layout($variables);
}
